import { get } from "get-es6";

const FULFILLED = "fulfilled";
const REJECTED = "rejected";

/**
 *
 * @param {*} promises - 2D Array of function promise
 * @param {*} callback
 */
const _provisionPromiseAllSettled = (promises2d, _result = []) => {
  // stop recursive
  if (promises2d.length === 0) {
    return _result;
  }

  // unpack promises
  const promises = get(promises2d, "0", []).map(
    (element) =>
      (typeof element.promise === "function" && element.promise()) ||
      element.promise
  );

  //recursive call the next level
  if (promises.length === 0) {
    return provisionPromiseAllSettled(promises2d.slice(1), _result);
  }

  return Promise.allSettled(promises)
    .then((results) => {
      results.forEach((result, idx) => {
        if (result.status === FULFILLED) {
          const { value } = result;
          const handler = get(promises2d, `0.${idx}.handler`);

          if (typeof handler === "function") {
            result.valueComputed = handler(value);
          }
        }
      });

      _result.push(results);

      if (results.some((result) => result.status === REJECTED)) {
        return _result;
      } else if (promises.length > 1) {
        return _provisionPromiseAllSettled(promises2d.slice(1), _result);
      }
    })
    .catch((err) => {
      throw err;
    });
};

export const provisionPromiseAllSettled = (promises) => {
  return _provisionPromiseAllSettled(promises)
    .then((results) => results)
    .catch((err) => {
      throw err;
    });
};
