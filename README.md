![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)
![KimDo7](https://img.shields.io/badge/KimDo7-red.svg)

### What is this repository for?

- Quick summary:
  This library is taking JS Promise to the next level. It is an evolutionary. It opens door to a completely new way of working with complex promises. It can easily handle a multiple layers of chaining promises. All this library needs from you is promises and its handlers.

- Version `1.0.0`

### Installation

`npm i provision-promise --save`

Then

```js
import { provisionPromiseAllSettled } from "provision-promise";
```

### Sample 1 | Easy Demo

```js
const promise1 = Promise.resolve(3);
const promise2 = new Promise((resolve) => setTimeout(resolve, 100, "foo"));
const promise3 = new Promise((resolve) => setTimeout(resolve, 1000, "foo"));
const promise4 = Promise.resolve(5);
const promise5 = Promise.reject(5);

const twice = (x) => x * 2;
const tripple = (x) => x * 3;

const promises = [
  [{ promise: promise1, handler: twice }, { promise: promise2 }],
  [
    { promise: promise3 },
    { promise: promise4, handler: tripple },
    { promise: promise5 },
  ],
];

// making request
provisionPromiseAllSettled(promises)
  .then((results) => results.forEach((result) => console.log(result)))
  .catch((err) => console.log(err));

/* Output:
[
  { status: 'fulfilled', value: 3, valueComputed: 6 },
  { status: 'fulfilled', value: 'foo' }
]
[
  { status: 'fulfilled', value: 'foo' },
  { status: 'fulfilled', value: 5, valueComputed: 15 },
  { status: 'rejected', reason: 5 }
]
*/
```
### Sample 2 | Provision Demo | Works well with apis call

```js
const promise1 = Promise.resolve(3);
const promise2 = new Promise((resolve) => setTimeout(resolve, 100, "foo"));
const promise3 = new Promise((resolve) => setTimeout(resolve, 1000, "foo"));
const promise4 = Promise.resolve(5);
const promise5 = Promise.resolve(5);

const twice = (x) => x * 2;
const tripple = (x) => x * 3;

const promises = [
  [{ promise: () => promise1, handler: twice }, { promise: () => promise2 }],
  [
    { promise: () => promise3 },
    { promise: promise4, handler: tripple },
    { promise: promise5 },
  ],
];

provisionPromiseAllSettled(promises)
  .then((results) => results.forEach((result) => console.log(result)))
  .catch((err) => console.log(err));

/* Output:
[
  { status: 'fulfilled', value: 3, valueComputed: 6 },
  { status: 'fulfilled', value: 'foo' }
]
[
  { status: 'fulfilled', value: 'foo' },
  { status: 'fulfilled', value: 5, valueComputed: 15 },
  { status: 'fulfilled', value: 5 }
]
*/
```
### Sample 3 | Easy Demo | Fail First Level

```js
const promise1 = Promise.reject(3);
const promise2 = new Promise((resolve) => setTimeout(resolve, 100, "foo"));
const promise3 = new Promise((resolve) => setTimeout(resolve, 1000, "foo"));
const promise4 = Promise.resolve(5);
const promise5 = Promise.reject(5);

const twice = (x) => x * 2;
const tripple = (x) => x * 3;

const promises = [
  [{ promise: promise1, handler: twice }, { promise: promise2 }],
  [
    { promise: promise3 },
    { promise: promise4, handler: tripple },
    { promise: promise5 },
  ],
];

provisionPromiseAllSettled(promises)
  .then((results) => results.forEach((result) => console.log(result)))
  .catch((err) => console.log(err));

/* Output:
[
  { status: 'rejected', reason: 3 },
  { status: 'fulfilled', value: 'foo' }
]
*/
```

### Who do I talk to?

- Repo owner or admin: `Kim Do`
- Linkedin: [@kimdo7](https://www.linkedin.com/in/kimdo7/)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)
